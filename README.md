# libpytempcovert
## Temperature unit converter library written in Python 3

This is a small script I wrote for personal usage, feel free to use it as you want.

## Licence
This repository, including all files, is licensed under the [MIT license](LICENCE) 
