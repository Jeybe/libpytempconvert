#!/usr/bin/python3
# coding: utf-8
import sys

class UnsupportedPythonVersion(Exception):
    pass

if sys.version_info[0] < 3:
    raise UnsupportedPythonVersion('Python 3 or newer is required')

# convert temperatures from one unit to another
# check wikipedia for the math
def temp_celsius_to_kelvin(temp_celsius):
    temp_kelvin = temp_celsius + 273.15
    return temp_kelvin

def temp_celsius_to_fahrenheit(temp_celsius):
    temp_fahrenheit = temp_celsius * 9/5 + 32
    return temp_fahrenheit

def temp_celsius_to_rankine(temp_celsius):
    temp_rankine = temp_celsius * 1.8 + 491.67
    return temp_rankine

def temp_celsius_to_newton(temp_celsius):
    temp_newton = temp_celsius * 0.33
    return temp_newton

def temp_celsius_to_delisle(temp_celsius):
    temp_delisle = (100 - temp_celsius) * 1.5
    return temp_delisle

def temp_celsius_to_reaumur(temp_celsius):
    temp_reaumur = temp_celsius * 0.8
    return temp_reaumur

def temp_celsius_to_romer(temp_celsius):
	temp_romer = temp_celsius * 21/40 + 7.5
	return temp_romer

def temp_kelvin_to_celsius(temp_kelvin):
    temp_celsius = temp_kelvin- 273.15
    return temp_celsius

def temp_kelvin_to_fahrenheit(temp_kelvin):
    temp_fahrenheit = temp_kelvin * 9/5 - 459.67
    return temp_fahrenheit

def temp_kelvin_to_rankine(temp_kelvin):
    temp_rankine = temp_kelvin * 9/5
    return temp_rankine

def temp_kelvin_to_newton(temp_kelvin):
    temp_newton = (temp_kelvin - 273.15) * 0.33
    return temp_newton

def temp_kelvin_to_delisle(temp_kelvin):
    temp_delisle = (373.15 - temp_kelvin) * 1.5
    return temp_delisle

def temp_kelvin_to_reaumur(temp_kelvin):
    temp_reaumur = (temp_kelvin - 273.15) * 0.8
    return temp_reaumur

def temp_kelvin_to_romer(temp_kelvin):
	temp_romer = (temp_kelvin - 273.15) * 21/40 + 7.5
	return temp_romer

def temp_fahrenheit_to_celsius(temp_fahrenheit):
    temp_celsius = (temp_fahrenheit - 32) * 5/9
    return temp_celsius

def temp_fahrenheit_to_kelvin(temp_fahrenheit):
    temp_kelvin = (temp_fahrenheit + 459.67) * 5/9
    return temp_kelvin

def temp_fahrenheit_to_rankine(temp_fahrenheit):
    temp_rankine = temp_fahrenheit + 459.67
    return temp_rankine

def temp_fahrenheit_to_newton(temp_fahrenheit):
    temp_newton = (temp_fahrenheit - 32) * (11/60)
    return temp_newton

def temp_fahrenheit_to_delisle(temp_fahrenheit):
    temp_delisle = (212 - temp_fahrenheit) * (5/6)
    return temp_delisle

def temp_fahrenheit_to_reaumur(temp_fahrenheit):
    temp_reaumur = (temp_fahrenheit - 32) * (4/9)
    return temp_reaumur

def temp_fahrenheit_to_romer(temp_fahrenheit):
	temp_romer = (temp_fahrenheit - 32) * 7/24 + 7.5
	return temp_romer

def temp_rankine_to_celsius(temp_rankine):
    temp_celsius = temp_rankine * 5/9 - 273.15
    return temp_celsius

def temp_rankine_to_kelvin(temp_rankine):
    temp_kelvin = temp_rankine * 5/9
    return temp_kelvin

def temp_rankine_to_fahrenheit(temp_rankine):
    temp_fahrenheit = temp_rankine - 459.67
    return temp_fahrenheit

def temp_rankine_to_newton(temp_rankine):
    temp_newton = (temp_rankine - 491.67) * (11/60)
    return temp_newton

def temp_rankine_to_delisle(temp_rankine):
    temp_delisle = (671.67 - temp_rankine) * (5/6)
    return temp_delisle

def temp_rankine_to_reaumur(temp_rankine):
    temp_reaumur = temp_rankine * (4/9) - 218.52
    return temp_reaumur

def temp_rankine_to_romer(temp_rankine):
	temp_romer = (temp_rankine - 491.67) * 7/24 + 7.5
	return temp_romer

def temp_newton_to_celsius(temp_newton):
    temp_celsius = temp_newton * (100/33)
    return temp_celsius

def temp_newton_to_kelvin(temp_newton):
    temp_kelvin = temp_newton * (100/33) + 273.15
    return temp_kelvin

def temp_newton_to_fahrenheit(temp_newton):
    temp_fahrenheit = temp_newton * (60/11) + 32
    return temp_fahrenheit

def temp_newton_to_rankine(temp_newton):
    temp_rankine = temp_newton * (60/11) + 491.67
    return temp_rankine

def temp_newton_to_delisle(temp_newton):
    temp_delisle = (33 - temp_newton) * (50/11)
    return temp_delisle

def temp_newton_to_reaumur(temp_newton):
    temp_reaumur = temp_newton * (80/33)
    return temp_reaumur

def temp_newton_to_romer(temp_newton):
	temp_romer = temp_newton * 35/22 + 7.5
	return temp_romer

def temp_delisle_to_celsius(temp_delisle):
    temp_celsius = 100 - temp_delisle * (2/3)
    return temp_celsius

def temp_delisle_to_kelvin(temp_delisle):
    temp_kelvin = 373.15 - temp_delisle * (2/3)
    return temp_kelvin

def temp_delisle_to_fahrenheit(temp_delisle):
    temp_fahrenheit = 212 - temp_delisle * 1.2
    return temp_fahrenheit

def temp_delisle_to_rankine(temp_delisle):
    temp_rankine = 671.67 - temp_delisle * 1.2
    return temp_rankine

def temp_delisle_to_newton(temp_delisle):
    temp_newton = 33 - temp_delisle * 0.22
    return temp_newton

def temp_delisle_to_reaumur(temp_delisle):
    temp_reaumur = 80 - temp_delisle * (8/15)
    return temp_reaumur

def temp_delisle_to_romer(temp_delisle):
	temp_romer = 60 - temp_delisle * 7/20
	return temp_romer

def temp_reaumur_to_celsius(temp_reaumur):
    temp_celsius = temp_reaumur * 1.25
    return temp_celsius

def temp_reaumur_to_kelvin(temp_reaumur):
    temp_kelvin = temp_reaumur * 1.25 + 273.15
    return temp_kelvin

def temp_reaumur_to_fahrenheit(temp_reaumur):
    temp_fahrenheit = temp_reaumur * 2.25 + 32
    return temp_fahrenheit

def temp_reaumur_to_rankine(temp_reaumur):
    temp_rankine = temp_reaumur * 2.25 + 491.67
    return temp_rankine

def temp_reaumur_to_newton(temp_reaumur):
    temp_newton = temp_reaumur * (33/80)
    return temp_newton

def temp_reaumur_to_delisle(temp_reaumur):
    temp_delisle = (80 - temp_reaumur) * 1.875
    return temp_delisle

def temp_reaumur_to_romer(temp_reaumur):
    temp_romer = temp_reaumur * (21/32) + 7.5
    return temp_romer

def temp_romer_to_celsius(temp_romer):
	temp_celsius = (temp_romer - 7.5) * 40/21
	return temp_celsius

def temp_romer_to_kelvin(temp_romer):
	temp_kelvin = (temp_romer - 7.5) * 40/21 + 273.15
	return temp_kelvin

def temp_romer_to_fahrenheit(temp_romer):
	temp_fahrenheit = (temp_romer - 7.5) * 24/7 + 32
	return temp_fahrenheit

def temp_romer_to_rankine(temp_romer):
	temp_rankine = (temp_romer - 7.5) * 24/7 + 491.67
	return temp_rankine

def temp_romer_to_newton(temp_romer):
	temp_newton = (temp_romer - 7.5) * 22/35
	return temp_newton

def temp_romer_to_delisle(temp_romer):
	temp_delisle = (60 - temp_romer) * 20/7
	return temp_delisle

def temp_romer_to_reaumur(temp_romer):
	temp_reaumur = (temp_romer - 7.5) * 32/21
	return temp_reaumur

# the magic begins
if __name__ == "__main__":
    pass
